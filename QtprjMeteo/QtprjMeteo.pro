QT       += core gui network charts sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    config.cpp \
    datetime.cpp \
    main.cpp \
    mainwindow.cpp \
    optionaffichage.cpp \
    serverquery.cpp \
    tcp_client.cpp \
    weatherdata.cpp

HEADERS += \
    config.h \
    datetime.h \
    mainwindow.h \
    optionaffichage.h \
    serverquery.h \
    tcp_client.h \
    weatherdata.h

FORMS += \
    mainwindow.ui \
    optionaffichage.ui

TRANSLATIONS += \
    Qt_FR.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ressources.qrc
