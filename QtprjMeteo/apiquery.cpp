#include "apiquery.h"



QUrl apiQuery::getUrlforecast() const
{
    return urlforecast;
}

QUrl apiQuery::getUrlweather() const
{
    return urlweather;
}

apiQuery::apiQuery(QString city,QString unit)
{
    this->setURL(city, unit);
}

QString apiQuery::getCity()
{
    return cityName;
}

void apiQuery::setCity( QString value)
{
    cityName = value;
}

QString apiQuery::getUnits()
{
    return units;
}

void apiQuery::setUnits( QString value)
{
    units = value;
}

void apiQuery::setURL(QString cityVal,QString unitVal)
{
    //**************** CONFIG API VARIABLES *********************************

    QString callType = "weather";

    this->setCity(cityVal);// Convert cityName to lowerCase
    this->setUnits(unitVal); //Kelvin("standard")-Fahren("imperial")-Celsius("metric")

    QString format = "JSON"; //JSON-XML-HTML
    QString apiKey = "a4fc42e2ed8f4e1bbdb22651e8fad48a";

    //*************** SET API URL REQUEST *********************************

    resUrl = "http://api.openweathermap.org/data/2.5/"+callType+"?"+
        "q="+cityName+
        "&units="+units+
        "&format="+format+
        "&appid="+apiKey+""; //Query on OpenWeather server

    //Pour requeter sur donnees venant de UI:
    // query + ui->widget->text()
    // Ex.: "&units="+( ui->widgetUnits->text())+ ...

    //autre methode de generation de l'url

    //url pour lapi weather :
    QUrlQuery query;
    QUrl url("http://api.openweathermap.org/data/2.5/weather");

    query.addQueryItem("q", cityName);
    query.addQueryItem("units", units);
    query.addQueryItem("format",format );
    query.addQueryItem("appid", apiKey);
    url.setQuery(query);
    urlweather=url;

    //url pour lapi forecastweather :
    QUrlQuery query1;
    QUrl url1("http://api.openweathermap.org/data/2.5/forecast/daily");
    query1.addQueryItem("q", cityName);
   // query1.addQueryItem("mode","json");
    query1.addQueryItem("units", units);
    query1.addQueryItem("cnt", "5");
    query1.addQueryItem("appid", apiKey);
    url1.setQuery(query1);

    urlforecast=url1;


}


