#ifndef APIQUERY_H
#define APIQUERY_H

#include <QDebug>
#include <QUrlQuery>

class apiQuery
{
private:
    QString cityName;
    QString units;
    QString resUrl;
    QUrl urlforecast;
    QUrl urlweather;

public:
    apiQuery(QString, QString);
    QString getCity();
    void setCity( QString);

    QString getUnits();
    void setUnits( QString);

    void setURL(QString, QString);


    QUrl getUrlforecast() const;
    QUrl getUrlweather() const;
};

#endif // APIQUERY_H
