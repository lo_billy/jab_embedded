#include "config.h"

config::config(QObject *parent) : QObject(parent)
{

}

QString config::getFormatHeure()
{
    return FormatHeure;
}

void config::setFormatHeure(const QString &value)
{
    FormatHeure = value;
}

QString config::getCurrentunit()
{
    return currentunit;
}

void config::setCurrentunit(const QString &value)
{
    currentunit = value;
}

QString config::getCurrentpolice()
{
    return currentpolice;
}

void config::setCurrentpolice(const QString &value)
{
    currentpolice = value;
}

QString config::getCurrentcity()const
{
    return currentcity;
}

void config::setCurrentcity(const QString &value)
{
    currentcity = value;
}

QString config::getCurrentcolor()
{
    return currentcolor;
}

void config::setCurrentcolor(const QString &value)
{
    currentcolor = value;
}

QString config::getIconweather()
{
    return iconweather;
}

void config::setIconweather(const QString &value)
{
    iconweather = value;
}
