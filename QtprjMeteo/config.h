#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>
#include <QWidget>

class config : public QObject
{
    Q_OBJECT
public:
    explicit config(QObject *parent = nullptr);

    QString getFormatHeure();
    void setFormatHeure(const QString &value);

    QString getCurrentunit();
    void setCurrentunit(const QString &value);

    QString getCurrentpolice();
    void setCurrentpolice(const QString &value);

    QString getCurrentcity() const;
    void setCurrentcity(const QString &value);

    QString getCurrentcolor();
    void setCurrentcolor(const QString &value);



    QString getIconweather() ;
    void setIconweather(const QString &value);

private:
    QString FormatHeure ;
    QString currentunit;
    QString currentpolice;
    QString currentcity;
    QString currentcolor;
    QString iconweather;

signals:



};

#endif // CONFIG_H
