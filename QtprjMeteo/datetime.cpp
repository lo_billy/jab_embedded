#include "datetime.h"

datetime::datetime(QObject *parent) : QObject(parent)
{

}

QString datetime::disp_Time(QString format)
{
    QString time;
    //At execution current time
    QTime currentT =  QTime::currentTime();
    //convert time base on format
    if(format=="12H"){
         time=currentT.toString("h:mm:ss ap");
    }
    else if (format=="24H"){
        time=currentT.toString("H:mm:ss");
    }
    return time;
}
QString datetime::disp_Date()
{
    //At execution current date
    QDateTime currentDate = QDateTime::currentDateTime();
    return currentDate.date().toString("ddd d MMMM yyyy");
    //QLocale().
}
