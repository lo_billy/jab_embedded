#ifndef DATETIME_H
#define DATETIME_H

#include <QObject>

#include <QTimer>
#include <QTime>
#include <QDateTime>

class datetime : public QObject
{
    Q_OBJECT
public:
    explicit datetime(QObject *parent = nullptr);
    //Display current time
    QString disp_Time(QString ="");
    //Display current date
    QString disp_Date();

signals:

};

#endif // DATETIME_H
