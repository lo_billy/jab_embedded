#include "mainwindow.h"
#include <QTranslator>
#include <QInputDialog>
#include <QApplication>
#include <QFile>
#include <QSettings>

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    QSettings maConfig(QStandardPaths::locate(QStandardPaths::DocumentsLocation,"param.ini"), QSettings::IniFormat);
    QString pathCSS = maConfig.value("format/cheminCSS").toString();

    QFile fileCSS(pathCSS);
    qDebug() << "Path " << pathCSS ;
        bool openOK = fileCSS.open(QIODevice::ReadOnly);

        if (openOK)
        {
            QString infosCSS = fileCSS.readAll();
            a.setStyleSheet(infosCSS);
            fileCSS.close();
        }

    QTranslator t;

    QStringList languages;
    languages << "French" << "English";

    QString lang = QInputDialog::getItem(NULL,"Choose a language","Language",languages);
    if( lang == "French"){
        QLocale curLocale(QLocale("fr"));
        QLocale::setDefault(curLocale);

        t.load(":/translation/tr_fr.qm");
        a.installTranslator(&t);
    }
    else if( lang == "English"){
        QLocale curLocale(QLocale("en"));
        QLocale::setDefault(curLocale);

        t.load(":/translation/tr_en.qm");
        a.installTranslator(&t);
    }
//    if( lang != "French"){
//        a.installTranslator(&t);
//    }

    MainWindow w;
    w.show();



    return a.exec();
}
