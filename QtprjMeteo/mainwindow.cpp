#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "optionaffichage.h"
#include "config.h"

#include <QMessageBox>

#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QStackedBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFile>
#include <QImageReader>
#include <QPixmap>

#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->gbx_fen1->setVisible(true);

    ui->gbx_fen2->setVisible(false);
    ui->gbx_meteoB->setVisible(false);

//on instencie la boite de dialogue avec les parametre
    option = new OptionAffichage();

// initialisation de la meteo avec parametre par defaut
    def = new config();
    QSettings maConfig(QStandardPaths::locate(QStandardPaths::DocumentsLocation,"param.ini"), QSettings::IniFormat);
    FormatHeure = maConfig.value("format/heure").toString();
    QString uni = maConfig.value("format/units").toString();
    QString citydefault = maConfig.value("format/ville").toString();
    QString icon = maConfig.value("format/icone").toString();
     pichum = maConfig.value("format/pichum").toString(); ;
     picpress = maConfig.value("format/picpress").toString(); ;
     pictemp = maConfig.value("format/pictemp").toString(); ;
qDebug()<<picpress;
    setBaliseMerimage();

    def->setCurrentcity(citydefault);
    def->setFormatHeure(FormatHeure);
    def->setCurrentunit(uni);
    def->setIconweather(icon);

    initweather(def);



    // initialision du contenu des combobox
       populateCityBox();



    //Display current time and date in *parent Widget
    date = new datetime();

    //Sets a timer for disp_Time() with connect
    timer = new QTimer();
    timer->setInterval(1000); //1000ms = 1 sec
    timer->start();

    timer1 = new QTimer();
    timer1->setInterval(60000); //60000ms = 60 sec
    timer1->start();

    //updateTime();


    //When 1 sec timer has passed -> update the date and time ()
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));

    //set the data and connection each time config option are saved
       connect(option,SIGNAL(configchanged(config *)),this, SLOT(initweather(config *)));

       // Update des données balise mer
       connect(timer1, SIGNAL(timeout()), this, SLOT(initconnection()));

    //initialise une première connexion avec La balise mer;
       timer->singleShot(4000, this, SLOT(initconnection()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete option;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox msgBox;
        msgBox.setText(tr("Quitter ?"));
        msgBox.setInformativeText(tr(" Etes vous sûr(e) de vouloir quitter ?"));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setIcon(QMessageBox::Icon::Question);
        msgBox.setDefaultButton(QMessageBox::Cancel);

        int reponse = msgBox.exec();

        switch(reponse)
        {
            case QMessageBox::Yes :
                    event->accept(); //Valider l'event de fermeture
                break;
            default:
                    event->ignore(); //Annuler l'event de fermeture
                break;
        }

}

void MainWindow::initconnection()
{
    client =new Tcp_client();
    client->tcp_connection();
    SetfromPi();
}

void MainWindow::setcurrentWeather()
{
    //Create URL with weather icon id
    QNetworkRequest req;
    QNetworkAccessManager *na = new QNetworkAccessManager;
    QNetworkReply *rep;
    QUrl newUrl;

    if(param_default==false){
        ui->lbl_ville->setText(option->getCurrentcity());
        ui->gbx_meteoV->setTitle(servQ->now.dayOfWeek());
        newUrl ="http://openweathermap.org/img/wn/"+servQ->now.weatherIcon()+"@2x.png";
        qDebug()<<"meteo icone"<<newUrl;
    }
    else{
         ui->lbl_ville->setText(def->getCurrentcity());
         ui->lbl_ville->adjustSize();
         qDebug()<<"currentcity"<<def->getCurrentcity();
         newUrl ="http://openweathermap.org/img/wn/"+def->getIconweather()+"@2x.png";
         param_default=false;
         qDebug()<<"meteo icone"<<newUrl;
    }
    req.setUrl(newUrl);
    rep = na->get(req); //.get() downloads a "content" Network Reply
    while(!rep->isFinished()) // While reply is false (not completed) continue to read
    {
        qApp->processEvents();
    }
    rep->deleteLater();

    QImageReader imaread(rep) ;
   // construire l'icone
    QImage pic = imaread.read();

    ui->label->setPixmap(QPixmap::fromImage(pic));
    ui->label->adjustSize();

    QString tp;
    if(servQ->getUnits()=="metric"){
        tp = tr(" °C");
    }
    else{
        tp = tr(" °F");
    }
    ui->lbl_currentMet->setText(servQ->now.temperature() + tp);
    ui->lbl_currentMet->adjustSize();
}

QChart *MainWindow::createBarChart(config *d) const
{
    QString alpha;
    if (d->getCurrentunit()=="metric"){
        alpha="°C";
    }
    else alpha="°F";
    QBarSet *set0 = new QBarSet(tr("Temperature Max ") + alpha);
    QBarSet *set1 = new QBarSet(tr("Temperature Min ") + alpha);
    QStringList categories;
    QList<double> MAX ;

    qDebug() << "count array " <<servQ->getTailleforecast();
    for (int i(0);i<servQ->forecastweatherset.count();i++)
        {
        weatherdata *yo = servQ->forecastweatherset.at(i);
        *set0 <<yo->temperature().toDouble();
        i+=1;
        MAX.append(yo->temperature().toDouble());
    }
    for (int i(1);i<servQ->getTailleforecast();i++)
        {
        weatherdata *ya = servQ->forecastweatherset.at(i);
        *set1 <<ya->temperature().toDouble();
        i+=1;
        qDebug() << "Jours semaine : " << ya->dayOfWeek();
        categories <<ya->dayOfWeek();
    }

//addition des bars
    QBarSeries *series = new QBarSeries();
    //QStackedBarSeries *series = new QStackedBarSeries();
    series->append(set0);
    series->append(set1);
    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle(servQ->getCityName()+ tr(" Previsions meteo prochains jours"));
    chart->setAnimationOptions(QChart::SeriesAnimations);

    //mise en place des categories du diagram
    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);
    QValueAxis *axisY = new QValueAxis();
    // determine la valeur max de l'axe y
    double max =*std::max_element(MAX.begin(),MAX.end());
    double min = -(max+5);
    axisY->setRange(min,max+5);
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);
    return chart;
}

void MainWindow::populateCityBox()
{
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    QString cod,country;
    QSettings maConfig(QStandardPaths::locate(QStandardPaths::DocumentsLocation,"param.ini"), QSettings::IniFormat);
    QString dbchemin = maConfig.value("format/chemindbcode").toString();
    qDebug()<<"voici le chemin"<<dbchemin;
    if (QFile(dbchemin).exists())
        {
        db.setDatabaseName(dbchemin);
        bool BdcOk = db.open();

        if (BdcOk == true)
            {
            //creation d'un tableau de donnée provenant de la base de donné where country_code='FR'

            QString reqQuery("select name, code From ISO_3166_Country_Code ORDER BY name;");
            QSqlQuery query(reqQuery,db);
            while(query.next()){
                 country = query.value(0).toString();
                 cod = query.value(1).toString();
                 QString ch = country;
                 QString ch2 =query.value(1).toString();;
                 ui->comboBox_pays->addItem(ch,ch2);
                 qDebug()<<ch;
                }
            }
        db.close();
        }
        else {
            qDebug() << "Echec Connexion !!!";
    }
}

void MainWindow::setBaliseMerimage()
{
    //ui->formLayout->addWidget(chartView, 1, 0)
    /*****************************Affichage des icons***************************************/
        QPixmap pix(pictemp); // load pixmap
        ui->label_T->setPixmap(pix);
        // get label dimensions
        int w =  ui->label_T->width();
        int h =  ui->label_T->height();
        // set a scaled pixmap to a w x h window keeping its aspect ratio
        ui->label_T->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
        QPixmap pix1(pichum);
         ui->label_h->setPixmap(pix1);
         int w1 =  ui->label_h->width();
         int h1 =  ui->label_h->height();
         ui->label_h->setPixmap(pix1.scaled(w1,h1,Qt::KeepAspectRatio));
         QPixmap pix2(picpress);
          ui->label_P->setPixmap(pix2);
          int w2 =  ui->label_P->width();
          int h2 =  ui->label_P->height();
          ui->label_P->setPixmap(pix2.scaled(w2,h2,Qt::KeepAspectRatio));
}


void MainWindow::SetfromPi()
{
    qDebug()<<"set les valeur "<<client->getList();

    //conversion des valeurs
    auto valtemp = client->getList().value(1).toDouble();
    auto valpress = client->getList().value(3).toDouble();
    auto valhumid = client->getList().value(5).toDouble();

    ui->progressBar_Hu->setValue(valhumid);

    //ui->label_Temp->adjustSize();
     ui->label_Temp->setText(tr(client->getList().value(0))+" : "+ QString::number(valtemp, 'f', 2) +tr(" °C"));
     ui->label_Temp->adjustSize();
     ui->label_press->setText(tr(client->getList().value(2))+" : "+QString::number(valpress,'f',2)+tr(" hPa"));
     ui->label_press->adjustSize();
     ui->label_humid->setText(tr(client->getList().value(4))+" : "+QString::number(valhumid,'f',2)+tr(" %"));
     ui->label_humid->adjustSize();
}

void MainWindow::updateTime()
{
    ui->lbl_date->setText(date->disp_Date());
    ui->lbl_time->setText(date->disp_Time(FormatHeure));
}

void MainWindow::updateWeather()
{
    QChartView *chartView ;
    //config curent = about->getCurentconfig();
    chartView = new QChartView(createBarChart(option->getCurentconfig()));
    chartView->setRenderHint(QPainter::Antialiasing);
    ui->gridLayout->addWidget(chartView, 1, 0);
    //update time format
    if(param_default==false)
    {
        FormatHeure=option->getCurrentformat();
    }
    // refresh de l'icone et des donnée
    setcurrentWeather();
}

void MainWindow::initweather(config *d)
{
    servQ = new serverQuery(d->getCurrentcity(),d->getCurrentunit());
    servQ->handleWeatherAPIdata(servQ->reply);

    updateWeather();

}



void MainWindow::on_btn_callAPI_clicked()
{
    ui->gbx_fen1->setVisible(false);
    ui->gbx_fen2->setVisible(true);
    ui->gbx_meteoB->setVisible(true);


        option->populatecityBox(ui->comboBox_pays->itemData(ui->comboBox_pays->currentIndex()).toString());

        option->exec();

    qDebug() << servQ->getUrlweather() << endl ; // Display query
    qDebug() <<"forcast"<< servQ->getUrlforecast() << endl ; // Display query

}


void MainWindow::on_actionQuitter_triggered()
{

    qDebug()<< "temp unit "<<option->getCurrentunit();

 close();
}

void MainWindow::on_actionOption_d_affichage_triggered()
{

    option->exec();
}



void MainWindow::on_actionrestart_triggered()
{

}


