#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QByteArray>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

#include <QDebug>
#include <QApplication>

#include "datetime.h"

#include "serverquery.h"

#include <QTimer>
#include "tcp_client.h"

#include <QtCharts/QChartGlobal>
#include <QWidget>
#include <QtCharts/QChartView>

#include "optionaffichage.h"
#include "config.h"


//QT_CHARTS_BEGIN_NAMESPACE
//class QChartView;
//class QChart;
//QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void closeEvent(QCloseEvent *) override;



private slots:
    void SetfromPi();
    //Callbutton openWeatherMap API
    void on_btn_callAPI_clicked();
    void updateTime();
    void updateWeather();
    void initweather(config *d);
    void initconnection();

    void setcurrentWeather();

    void on_actionQuitter_triggered();

    void on_actionOption_d_affichage_triggered();

    void on_actionrestart_triggered();


private:
    Ui::MainWindow *ui;
    QTimer *timer,*timer1; //Timer triggers process with time interval
    datetime *date;
    serverQuery *servQ;
    QTcpSocket *socket;
    Tcp_client *client;
    //variable intermediaire de config
    config *def;
    //boite de dialogue pour config
    OptionAffichage *option;

    //variable pour controler
    bool click=false,param_default=true;

    QChart *createBarChart(config *d) const;
    void populateCityBox();
    void setBaliseMerimage();


    QString FormatHeure = "" ;
    QString pichum = "" ;
    QString picpress = "" ;
    QString pictemp = "" ;



};

#endif // MAINWINDOW_H
