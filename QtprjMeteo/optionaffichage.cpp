#include "optionaffichage.h"
#include "ui_optionaffichage.h"
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFile>
#include <QDebug>
#include <QStandardPaths>

OptionAffichage::OptionAffichage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionAffichage)
{
    ui->setupUi(this);
    QSettings maConfig(QStandardPaths::locate(QStandardPaths::DocumentsLocation,"param.ini"), QSettings::IniFormat);
    dbPath = maConfig.value("format/chemindb").toString();
    curentconfig = new config();
    populateUnitBox();
  //  populatecityBox();

}

OptionAffichage::~OptionAffichage()
{
    delete ui;
}

QString OptionAffichage::getCurrentunit()
{

    currentunit = ui->comboBox_unit->itemData(ui->comboBox_unit->currentIndex()).toString();
    return currentunit;
}

QString OptionAffichage::getCurrentformat()
{
   // currentformat = ui->comboBox_heure->itemData(ui->comboBox_heure->currentIndex()).toString();
    return currentformat;
}

QString OptionAffichage::getCurrentpolice()
{
    currentpolice=ui->fontComboBox->currentFont().toString();
    return currentpolice;
}

QString OptionAffichage::getCurrentcity()
{
   currentcity= ui->comboBox_city->itemData(ui->comboBox_city->currentIndex()).toString();
    return currentcity;
}

QString OptionAffichage::getCurrentcolor()
{
    return currentcolor;
}


void OptionAffichage::populateUnitBox()
{
    ui->comboBox_unit->addItem("Celsius","metric");
    ui->comboBox_unit->addItem("Farenheit","imperial");
}

void OptionAffichage::populatecityBox(QString country_cod)
{
    qDebug() << "code pays selectionné !!!"<<country_cod;
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    QString city,country,requete;

    if (QFile(dbPath).exists())
        {
        db.setDatabaseName(dbPath);
        bool BdcOk = db.open();

        if (BdcOk == true)
            {
            //creation d'un tableau de donnée provenant de la base de donner where country_code='FR'
            requete="select city, country_code From OWM_city_country_code where country_code='";
            requete=requete+country_cod;
            requete= requete+"';";
            QString reqQuery(requete);
            QSqlQuery query(reqQuery,db);
            while(query.next()){
                 city = query.value(0).toString();
                 country = query.value(1).toString();
                 QString ch = city +","+country;
                 QString ch2 = city +","+query.value(1).toString();;
                //qDebug()<<ch;
                ui->comboBox_city->addItem(ch,ch2);

                }
            }
        db.close();

        }
        else {
            qDebug() << "Echec Connexion !!!";
        }
}

void OptionAffichage::saveconfig()
{
    QSettings maConfig(QStandardPaths::locate(QStandardPaths::DocumentsLocation,"param.ini"), QSettings::IniFormat);
    //FormatHeure = maConfig.value("format/heure").toString();
    maConfig.setValue("format/ville",getCurrentcity());
    maConfig.setValue("format/units",getCurrentunit());

    if(getCurrentformat()!=""){
    maConfig.setValue("format/heure",getCurrentformat());}
    else {
        maConfig.setValue("format/heure","24H");
    }
    maConfig.setValue("format/police",getCurrentpolice());
}




void OptionAffichage::on_buttonBox_accepted()
{
    //update la police !
    QApplication::setFont(ui->fontComboBox->currentFont());
    foreach (QWidget *widget, QApplication::allWidgets()) {
            widget->setFont(ui->fontComboBox->currentFont());
            widget->update();
        }

    saveconfig();
   qDebug() << "actual font"<<ui->fontComboBox->currentFont();
    setCurentconfig();
    emit configchanged(curentconfig);


}

void OptionAffichage::on_rdb_12h_clicked()
{
    currentformat="12H";
}

void OptionAffichage::on_rdb_24h_clicked()
{
    currentformat="24H";
}



void OptionAffichage::setCurentconfig()
{
    curentconfig->setCurrentcity(getCurrentcity());
    curentconfig->setCurrentunit(getCurrentunit());
    curentconfig->setFormatHeure(getCurrentformat());
    curentconfig->setCurrentpolice(getCurrentpolice());
    curentconfig->setCurrentcolor(getCurrentcolor());

}

config *OptionAffichage::getCurentconfig()
{
    return curentconfig;
}
