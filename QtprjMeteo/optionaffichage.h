#ifndef OPTIONAFFICHAGE_H
#define OPTIONAFFICHAGE_H

#include <QAbstractButton>
#include <QDialog>
#include "config.h"

namespace Ui {
class OptionAffichage;
}

class OptionAffichage : public QDialog
{
    Q_OBJECT

public:
    explicit OptionAffichage(QWidget *parent = nullptr);
    ~OptionAffichage();

    QString getCurrentunit();

    QString getCurrentformat();

    QString getCurrentpolice();

    QString getCurrentcity();

    QString getCurrentcolor();

    config *getCurentconfig() ;

    void setCurentconfig();
    void populatecityBox(QString country_cod);


private slots:
    // void on_buttonBox_clicked(QAbstractButton *button);

    void on_buttonBox_accepted();

    void on_rdb_12h_clicked();

    void on_rdb_24h_clicked();

signals :
   void configchanged(config *);

private:
    Ui::OptionAffichage *ui;
    QString currentformat;
    QString currentunit;
    QString currentpolice;
    QString currentcity;

    QString currentcolor;
    config *curentconfig;
    QString dbPath;

private:
    void populateUnitBox();
    void populatepoliceBox();
    void populatecolorBox();

    void saveconfig();


};

#endif // OPTIONAFFICHAGE_H
