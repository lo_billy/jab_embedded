#include "serverquery.h"

serverQuery::serverQuery(QString city,QString unit, QObject *parent)
    :QObject(parent)
{
    QString apiKey = "a4fc42e2ed8f4e1bbdb22651e8fad48a";
    setCityName(city);
    setUnits(unit);
    //url pour lapi weather :
    QUrlQuery query;
    QUrl url("http://api.openweathermap.org/data/2.5/weather");

    query.addQueryItem("q", cityName);
    query.addQueryItem("units", units);
    query.addQueryItem("format","json" );
    query.addQueryItem("appid", apiKey);
    url.setQuery(query);
    urlweather=url;

    request.setUrl(url); //Query is created here
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    queryConnect();
}

void serverQuery::queryConnect()
{
    reply = nam->get(request); //.get() downloads a "content" Network Reply

    while(!reply->isFinished()) // While reply is false (not completed) continue to read
    {
        qApp->processEvents();
    }
    reply->deleteLater(); // Pending method - When process finished Delete Network Reply pointer to free memory

}

void serverQuery::handleWeatherAPIdata(QNetworkReply *replyobj)
{
    QJsonDocument jsonResponse = QJsonDocument::fromJson(replyobj->readAll());
    if (jsonResponse.isObject()) {
                QJsonObject obj = jsonResponse.object();
                QJsonObject tempObject;
                QJsonValue val;
                if (obj.contains(QStringLiteral("weather"))) {
                    val = obj.value(QStringLiteral("weather"));
                    QJsonArray weatherArray = val.toArray();
                    val = weatherArray.at(0);
                    tempObject = val.toObject();
                    setWeatherDescription(tempObject.value(QStringLiteral("description")).toString());
                    setWeatherIcon(tempObject.value("icon").toString());
                    //stock les data dans la variable weather actu now
                    now.setWeatherDescription(tempObject.value(QStringLiteral("description")).toString());
                    now.setWeatherIcon(tempObject.value("icon").toString());
                }
                if (obj.contains(QStringLiteral("main"))) {
                    val = obj.value(QStringLiteral("main"));
                    tempObject = val.toObject();
                    val = tempObject.value(QStringLiteral("temp"));
                    QVariant temp = val.toVariant();
                    setTemperature(temp.toString());
                    //stock les data dans la variable weather actu now
                    now.setTemperature(temp.toString());
                }
                if (obj.contains(QStringLiteral("dt"))) {
                    val = obj.value(QStringLiteral("dt"));
                    QDateTime dt = QDateTime::fromMSecsSinceEpoch((qint64)val.toDouble()*1000);
                    //stock les data dans la variable weather actu now
                    now.setDayOfWeek(QLocale().dayName(dt.date().dayOfWeek()));
                }
            }

    //preparation url pour lapi forecastweather :
    QString apiKeyforecast = "a4fc42e2ed8f4e1bbdb22651e8fad48a";

    QUrlQuery query;
    QUrl url1("http://api.openweathermap.org/data/2.5/forecast");
    query.addQueryItem("q", cityName);
    query.addQueryItem("mode","json");
    query.addQueryItem("units", units);
    //query.addQueryItem("cnt", "5");
    query.addQueryItem("appid", apiKeyforecast);
    url1.setQuery(query);

    urlforecast=url1;
    request.setUrl(url1); //Query is created here
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    queryConnect();
    handleForecastAPIdata(reply);

}

void serverQuery::handleForecastAPIdata(QNetworkReply *replyobj)
{
qDebug() << "description ";
QJsonDocument jsonResponse = QJsonDocument::fromJson(replyobj->readAll());
qDebug() << "jsonResponse vide :" << jsonResponse.isEmpty();
//qDebug() << jsonResponse;

//if (jsonResponse.isObject()) {
            QJsonObject obj = jsonResponse.object();

            QJsonObject forcastObject,objmain,sublist;
            QJsonValue val,val1;
            val=obj.value(QString("list"));
            QJsonArray v=val.toArray();
qDebug() << "Pair keys of  " << v.size();
            if (val.isArray()) {
                QJsonArray weatherArray = val.toArray();
qDebug() << "Pair keys of  " << weatherArray.size();
                QString data="";
                for(int i = 0; i<weatherArray.size(); i++){
                    weatherdata *forecastEntry = new weatherdata();

                    //min/max temperature
                    QJsonObject subtree = weatherArray.at(i).toObject();
                    forcastObject = subtree.value(QString("main")).toObject();
                    val=forcastObject.value(QString("temp"));
                    QVariant transi = val.toVariant();
                    data = transi.toString();
                    forecastEntry->setTemperature(data);

                    //get date
                    val = subtree.value(QStringLiteral("dt"));
                    QDateTime dt = QDateTime::fromMSecsSinceEpoch((qint64)val.toDouble()*1000);
                    //forecastEntry->setDayOfWeek(dt.date().toString(QStringLiteral("ddd")));
                    forecastEntry->setDayOfWeek(QLocale().dayName(dt.date().dayOfWeek()));

                    //get icon
                    QJsonArray ArrayWEATHER = subtree.value(QStringLiteral("weather")).toArray();
                     forcastObject = ArrayWEATHER.at(0).toObject();
                    forecastEntry->setWeatherIcon(forcastObject.value(QStringLiteral("icon")).toString());

                     //get description
                     forecastEntry->setWeatherDescription(forcastObject.value(QStringLiteral("description")).toString());

                    forecastweathersetbrut.append(forecastEntry);

                }
                qDebug() << "count array " <<forecastweathersetbrut.count();

            }
            setTempMax();
       // }

}

QString serverQuery::getWeatherDescription() const
{
    return WeatherDescription;
}

void serverQuery::setWeatherDescription(const QString &value)
{
    WeatherDescription = value;
}

QString serverQuery::getWeatherIcon() const
{
    return WeatherIcon;
}

void serverQuery::setWeatherIcon(const QString &value)
{
    WeatherIcon = value;
}

QString serverQuery::getTemperature() const
{
    return TemperatureString;
}

void serverQuery::setTemperature(const QString &value)
{
    TemperatureString = value;
}

int serverQuery::getTailleforecast() const
{
    return tailleforecast;
}

QUrl serverQuery::getUrlforecast() const
{
    return urlforecast;
}

QUrl serverQuery::getUrlweather() const
{
    return urlweather;
}

QString serverQuery::getCityName() const
{
    return cityName;
}

void serverQuery::setCityName(const QString &value)
{
    cityName = value;
}

QString serverQuery::getUnits() const
{
    return units;
}

void serverQuery::setUnits(const QString &value)
{
    units = value;
}

QString serverQuery::getTempMax() const
{


    return tempMax;
}

void serverQuery::setTempMax()
{
    QList<double> TempbrutMax;
    int j=0,q=0,k=0;
    for(int i=0;i<forecastweathersetbrut.size();i++){

           weatherdata *forecastIn = new weatherdata();
           weatherdata *forecast = new weatherdata();
           //recupère la meteo à comparer
           forecastIn=forecastweathersetbrut.at(0+j);
           forecast=forecastweathersetbrut.at(i-k);

        if (forecastIn->dayOfWeek()==forecast->dayOfWeek() && now.dayOfWeek() != forecast->dayOfWeek() )
        {
            QList<QString> Tempbrut;
            QString tp = forecast->temperature();
           // rajoute dans la liste de temperature à comparer
           TempbrutMax.append(tp.toDouble());
           //calcul du min et du max de la temperature sur une journée
           double max =*std::max_element(TempbrutMax.begin(),TempbrutMax.end());
           double min =*std::min_element(TempbrutMax.begin(),TempbrutMax.end());
            k=0;
           if (forecast->temperature().toDouble() == max)
           { // ajout de la meteo max
               map.insert(q,forecast);
           }
           if(forecast->temperature().toDouble() == min)
           {
               // ajout de la meteo min
               map.insert(q+1,forecast);
           }
        }
        else{
            j=i;
            TempbrutMax.clear();//rafraichis la liste avant une nouvelle comparaison
            q=map.size();
            k=1;
        }
    }
    // on range les prevision par max / min dans une liste
    foreach(int i, map.keys()){
        forecastweatherset.append(map[i]);
    }
    tailleforecast=forecastweatherset.size();
}


