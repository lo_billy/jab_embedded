//******************* SET SERVER CONNECTION *********************************

#ifndef SERVERQUERY_H
#define SERVERQUERY_H

#include <QObject>
#include <QtNetwork>
#include <QUrlQuery>

#include "weatherdata.h"

class serverQuery : public QObject
{
    Q_OBJECT
public:
    explicit serverQuery(QString="",QString="", QObject *parent = nullptr);
    void queryConnect();
    void handleWeatherAPIdata(QNetworkReply *replyobj);
    void handleForecastAPIdata(QNetworkReply *replyobj);

    QNetworkReply *reply;



    QString getWeatherDescription() const;
    void setWeatherDescription(const QString &value);

    QString getWeatherIcon() const;
    void setWeatherIcon(const QString &value);

    QString getTemperature() const;
    void setTemperature(const QString &value);

    QList<weatherdata*> forecastweathersetbrut;
    QList<weatherdata*> forecastweatherset;

    int tailleforecast;

    //Meteo du jour
    weatherdata now;

    QUrl getUrlforecast() const;

    QUrl getUrlweather() const;

    QString getCityName() const;
    void setCityName(const QString &value);

    QString getUnits() const;
    void setUnits(const QString &value);

    QString getTempMax() const;
    void setTempMax();

    int getTailleforecast() const;

signals:

private:
    QNetworkRequest request;
    //Class that manages Queries, Network access and ".GET() contents method"
    QNetworkAccessManager *nam = new QNetworkAccessManager;

    QString WeatherDescription;
    QString WeatherIcon;
    QString TemperatureString;


    QString cityName;
    QString units;
    QString resUrl;
    QUrl urlforecast;
    QUrl urlweather;
    QMap<int,weatherdata*> map;

  QString tempMax;

};


#endif // SERVERQUERY_H
