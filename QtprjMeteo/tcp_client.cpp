#include "tcp_client.h"

Tcp_client::Tcp_client(QObject *parent) : QObject(parent)
{

}

QList<QByteArray> Tcp_client::getList() const
{
      return list;
}

void Tcp_client::tcp_connection()
{
    qDebug() << "État de la socket :" << socket->state();

    qDebug() << "Connexion au serveur";


    //connection au raspberry pi 3

    socket->connectToHost("82.65.244.166", 12001);

     if(socket->waitForConnected(5000))
         {
             // send
             socket->write("Connecté au raspberry\r\n\r\n");
             socket->waitForBytesWritten();
             socket->waitForReadyRead();

             while(socket->bytesAvailable()==0){
                socket->close();
                socket->connectToHost("82.65.244.166", 12001);

                if(socket->waitForConnected(5000)){
                 socket->waitForBytesWritten();
                 socket->waitForReadyRead();

                }
             }

             // get the data
            list = socket->readAll().split(':');

            // close the connection
             socket->close();
         }
         else
         {
             qDebug() << "Not connected!";
         }
}
