#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QtNetwork>

class Tcp_client : public QObject
{
    Q_OBJECT
public:
    explicit Tcp_client(QObject *parent = nullptr);


    QList<QByteArray> getList() const;

signals:

public slots:

    void tcp_connection();

private:

    QTcpSocket *socket = new QTcpSocket(this);
    QList<QByteArray> list;

};

#endif // TCP_CLIENT_H
