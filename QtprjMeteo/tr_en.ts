<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>City &amp; Sea Weather Forecast App</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="28"/>
        <source>Current Date</source>
        <translation>Current date</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <source>Current Time</source>
        <translation>Current time</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <source>Météo ville</source>
        <translation>Current city weather</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <source>Ville</source>
        <translation>City</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="83"/>
        <source>description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="90"/>
        <source>Météo du jour</source>
        <translation>Weather</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="102"/>
        <source>Prévision</source>
        <translation>5 days forecast</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Balise Mer</source>
        <translation>Sea weather beacon</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="125"/>
        <source>Pression</source>
        <translation>Pressure</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <source>humidité</source>
        <translation>Humidity</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="146"/>
        <source>Temperature</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <source>Appel Meteo</source>
        <translation>Launch weather request</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>Menu</source>
        <translation>&amp;Menu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <source>Quitter</source>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="211"/>
        <source>Option d&apos;affichage</source>
        <translation>&amp;Setup menu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <location filename="mainwindow.ui" line="219"/>
        <source>restart</source>
        <translation>Re&amp;boot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="97"/>
        <source>Quitter ?</source>
        <translation>Exit ?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="98"/>
        <source> Etes vous sûr(e) de vouloir quitter ?</source>
        <translation>Are you sure ?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <location filename="mainwindow.cpp" line="306"/>
        <location filename="mainwindow.cpp" line="347"/>
        <source> °C</source>
        <translation> °C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="168"/>
        <location filename="mainwindow.cpp" line="350"/>
        <source> °F</source>
        <translation> °F</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="186"/>
        <source>Temperature Max </source>
        <translation>High temp.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="187"/>
        <source>Temperature Min </source>
        <translation>Low temp.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="219"/>
        <source> Previsions meteo prochains jours</source>
        <translation> - in the next 5 days</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="308"/>
        <source> hPa</source>
        <translation> hPa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="310"/>
        <source> %</source>
        <translation> %</translation>
    </message>
</context>
<context>
    <name>OptionAffichage</name>
    <message>
        <location filename="optionaffichage.ui" line="14"/>
        <source>Dialog</source>
        <translation>Setup Menu</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="24"/>
        <source>Format horaire defaut:</source>
        <translation>Time format</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="32"/>
        <source>12H</source>
        <translation>12H</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="39"/>
        <source>24h</source>
        <translation>24H</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="54"/>
        <source>Police :</source>
        <translation>Fonts :</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="64"/>
        <source> Units :</source>
        <translation>Units :</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="78"/>
        <source>couleur :</source>
        <translation>Colour :</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="109"/>
        <source>Ville : </source>
        <translation>Select a City :</translation>
    </message>
</context>
</TS>
