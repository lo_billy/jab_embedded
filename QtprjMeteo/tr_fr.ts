<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Application Météo Ville &amp; Maritime</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="28"/>
        <source>Current Date</source>
        <translation>Date du jour</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <source>Current Time</source>
        <translation>Heure actuelle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <source>Météo ville</source>
        <translation>Météo de ville en cours</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <source>Ville</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="83"/>
        <source>description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="90"/>
        <source>Météo du jour</source>
        <translation>Météo du jour</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="102"/>
        <source>Prévision</source>
        <translation>Prévisions sur 5 jours</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Balise Mer</source>
        <translation>Balise maritime</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="125"/>
        <source>Pression</source>
        <translation>Pression</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <source>humidité</source>
        <translation>Humidité</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="146"/>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <source>Appel Meteo</source>
        <translation>Appel météo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>Menu</source>
        <translation>&amp;Menu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <source>Quitter</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="211"/>
        <source>Option d&apos;affichage</source>
        <translation>O&amp;ptions d&apos;affichage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <location filename="mainwindow.ui" line="219"/>
        <source>restart</source>
        <translation>&amp;Redémarrage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="97"/>
        <source>Quitter ?</source>
        <translation>Quitter ?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="98"/>
        <source> Etes vous sûr(e) de vouloir quitter ?</source>
        <translation>Etes-vous sûr(e) de vouloir quitter ?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <location filename="mainwindow.cpp" line="306"/>
        <location filename="mainwindow.cpp" line="347"/>
        <source> °C</source>
        <translation> °C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="168"/>
        <location filename="mainwindow.cpp" line="350"/>
        <source> °F</source>
        <translation> °F</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="186"/>
        <source>Temperature Max </source>
        <translation>Temp. max</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="187"/>
        <source>Temperature Min </source>
        <translation>Temp. min</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="219"/>
        <source> Previsions meteo prochains jours</source>
        <translation> - sur les 5 prochains jours</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="308"/>
        <source> hPa</source>
        <translation> hPa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="310"/>
        <source> %</source>
        <translation> %</translation>
    </message>
</context>
<context>
    <name>OptionAffichage</name>
    <message>
        <location filename="optionaffichage.ui" line="14"/>
        <source>Dialog</source>
        <translation>Options d&apos;affichage</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="24"/>
        <source>Format horaire defaut:</source>
        <translation>Format de l&apos;heure</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="32"/>
        <source>12H</source>
        <translation>12h</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="39"/>
        <source>24h</source>
        <translation>24h</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="54"/>
        <source>Police :</source>
        <translation>Police :</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="64"/>
        <source> Units :</source>
        <translation>Unités :</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="78"/>
        <source>couleur :</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <location filename="optionaffichage.ui" line="109"/>
        <source>Ville : </source>
        <translation>Choix de la ville :</translation>
    </message>
</context>
</TS>
