#include "weatherdata.h"

weatherdata::weatherdata(QObject *parent) : QObject(parent)
{

}
weatherdata::weatherdata(const weatherdata &other):QObject(0),
    m_dayOfWeek(other.m_dayOfWeek),
    m_weather(other.m_weather),
    m_weatherDescription(other.m_weatherDescription),
    m_temperature(other.m_temperature)
{

}

QString weatherdata::dayOfWeek() const
{
    return m_dayOfWeek;
}

QString weatherdata::weatherIcon() const
{
    return m_weather;
}

QString weatherdata::weatherDescription() const
{
    return m_weatherDescription;
}

QString weatherdata::temperature() const
{
    return m_temperature;
}

void weatherdata::setDayOfWeek(const QString &value)
{
    m_dayOfWeek = value;
}

void weatherdata::setWeatherIcon(const QString &value)
{
    m_weather = value;
}

void weatherdata::setWeatherDescription(const QString &value)
{
    m_weatherDescription = value;
}

void weatherdata::setTemperature(const QString &value)
{
    m_temperature = value;
}
