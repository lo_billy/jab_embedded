#ifndef WEATHERDATA_H
#define WEATHERDATA_H

#include <QObject>

class weatherdata : public QObject
{
    Q_OBJECT
public:
    explicit weatherdata(QObject *parent = nullptr);
    weatherdata(const weatherdata &other);

    QString dayOfWeek() const;
    QString weatherIcon() const;
    QString weatherDescription() const;
    QString temperature() const;

    void setDayOfWeek(const QString &value);
    void setWeatherIcon(const QString &value);
    void setWeatherDescription(const QString &value);
    void setTemperature(const QString &value);

private:
    QString m_dayOfWeek;
    QString m_weather;
    QString m_weatherDescription;
    QString m_temperature;
};

#endif // WEATHERDATA_H
